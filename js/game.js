//Debugging
var myClipy = 850,
    myDestX = 150,
    myDestY = 400,
    myOffsetX = 0,
    myOffsetY = -2,
    myAddSegment = 1;

    //=========================================================================
    // minimalist DOM helpers
    //=========================================================================    
    var winWidth, 
        winHeight, 
        gameBody, 
        mainMenu, 
        pauseScreen, 
        gameOver, 
        highScore, 
        healthBar, 
        collisionSprite, 
        engineSound, 
        engineRunning,
        myHud = {
          noCollisionPower : false,
          reset : function (argument) {
            noCollisionPower = false;
          },
          initWatchListeners : function (argument) {
            this.watch("noCollisionPower", function (id, oldval, newval) {
              if(oldval===false){
                setTimeout(function (argument) {
                  myHud.noCollisionPower = false
                }, 5000);
              }
            });
          }
        }
        Dom = {

            get:  function (id) { return ((id instanceof HTMLElement) || (id === document)) ? id : document.getElementById(id); },
            set:  function (id, html) { Dom.get(id).innerHTML = html;                        },
            on:   function (ele, type, fn, capture) { Dom.get(ele).addEventListener(type, fn, capture);    },
            un:   function (ele, type, fn, capture) { Dom.get(ele).removeEventListener(type, fn, capture); },
            show: function (ele, type) { Dom.get(ele).style.display = (type || 'block');      },
            blur: function (ev) { ev.target.blur();                                    },
            addClassName:    function (ele, name) { Dom.toggleClassName(ele, name, true);  },
            removeClassName: function (ele, name) { Dom.toggleClassName(ele, name, false); },
            toggleClassName: function (ele, name, on) {
                ele = Dom.get(ele);
                var classes = ele.className.split(' '), n = classes.indexOf(name);
                on = (typeof on === 'undefined') ? (n < 0) : on;
                if (on && (n < 0)) {
                    classes.push(name);
                } else if (!on && (n >= 0)) {
                    classes.splice(n, 1);
                }
                ele.className = classes.join(' ');
            },
            muted: false,
            storage: window.localStorage || {}
        }
    //=========================================================================
    // general purpose helpers (mostly math)
    //=========================================================================
    var Util = {

      timestamp:        function ()                  { return new Date().getTime();                                    },
      toInt:            function (obj, def)          { if (obj !== null) { var x = parseInt(obj, 10); if (!isNaN(x)) return x; } return Util.toInt(def, 0); },
      toFloat:          function (obj, def)          { if (obj !== null) { var x = parseFloat(obj);   if (!isNaN(x)) return x; } return Util.toFloat(def, 0.0); },
      limit:            function (value, min, max)   { return Math.max(min, Math.min(value, max));                     },
      randomInt:        function (min, max)          { return Math.round(Util.interpolate(min, max, Math.random()));   },
      randomChoice:     function (options)           { return options[Util.randomInt(0, options.length-1)];            },
      percentRemaining: function (n, total)          { return (n%total)/total;                                         },
      accelerate:       function (v, accel, dt)      { return v + (accel * dt);                                        },
      interpolate:      function (a,b,percent)       { return a + (b-a)*percent                                        },
      easeIn:           function (a,b,percent)       { return a + (b-a)*Math.pow(percent,2);                           },
      easeOut:          function (a,b,percent)       { return a + (b-a)*(1-Math.pow(1-percent,2));                     },
      easeInOut:        function (a,b,percent)       { return a + (b-a)*((-Math.cos(percent*Math.PI)/2) + 0.5);        },
      exponentialFog:   function (distance, density) { return 1 / (Math.pow(Math.E, (distance * distance * density))); },

      increase:  function (start, increment, max) { // with looping
        var result = start + increment;
        while (result >= max)
          result -= max;
        while (result < 0)
          result += max;
        return result;
      },

      project: function (p, cameraX, cameraY, cameraZ, cameraDepth, width, height, roadWidth) {
        p.camera.x     = (p.world.x || 0) - cameraX;
        p.camera.y     = (p.world.y || 0) - cameraY;
        p.camera.z     = (p.world.z || 0) - cameraZ;
        p.screen.scale = cameraDepth/p.camera.z;
        p.screen.x     = Math.round((width/2)  + (p.screen.scale * p.camera.x  * width/2));
        p.screen.y     = Math.round((height/2) - (p.screen.scale * p.camera.y  * height/2));
        p.screen.w     = Math.round(             (p.screen.scale * roadWidth   * width/2));
      },

      overlap: function (x1, w1, x2, w2, percent) {
        var half = (percent || 1)/2;
        var min1 = x1 - (w1*half);
        var max1 = x1 + (w1*half);
        var min2 = x2 - (w2*half);
        var max2 = x2 + (w2*half);
        
        return (myHud.noCollisionPower===true)? false : (!((max1 < min2) || (min1 > max2)));
      },

      health : 4,
      reduceHealth:   function () {
        this.health--;
        // console.log(this.health)
        healthBar.className = "health"+(this.health*25);
      }

    }

    //=========================================================================
    // POLYFILL for requestAnimationFrame
    //=========================================================================

    if (!window.requestAnimationFrame) { // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
      window.requestAnimationFrame = window.webkitRequestAnimationFrame || 
                                     window.mozRequestAnimationFrame    || 
                                     window.oRequestAnimationFrame      || 
                                     window.msRequestAnimationFrame     || 
                                     function (callback, element) {
                                       window.setTimeout(callback, 1000 / 60);
                                     }
    }

    //=========================================================================
    // GAME LOOP helpers
    //=========================================================================
    var Game = {  

      run: function (options) {

        Game.loadImages(options.images, function (images) {

          options.ready(images); // tell caller to initialize itself because images are loaded and we're ready to rumble

          Game.setKeyListener(options.keys);

          var canvas = options.canvas,    // canvas render target is provided by caller
              update = options.update,    // method to update game logic is provided by caller
              render = options.render,    // method to render the game is provided by caller
              step   = options.step,      // fixed frame step (1/fps) is specified by caller
              // stats  = options.stats,     // stats instance is provided by caller
              now    = null,
              last   = Util.timestamp(),
              dt     = 0,
              gdt    = 0;

          function frame() {
            now = Util.timestamp();
            dt  = Math.min(1, (now - last) / 1000); // using requestAnimationFrame have to be able to handle large delta's caused when it 'hibernates' in a background or non-visible tab
            gdt = gdt + dt;
            while (gdt > step) {
              gdt = gdt - step;
              update(step);
            }
            render();
            // stats.update();
            last = now;
            requestAnimationFrame(frame, canvas);
          }
          frame(); // lets get this party started
        });
      },

      //---------------------------------------------------------------------------

      loadImages: function (names, callback) { // load multiple images and callback when ALL images have loaded
        var result = [];
        var count  = names.length;

        var onload = function () {
          if (--count == 0)
            callback(result);
        };

        for(var n = 0 ; n < names.length ; n++) {
          var name = names[n];
          result[n] = document.createElement('img');
          Dom.on(result[n], 'load', onload);
          result[n].src = "images/" + name + ".png";
        }
      },

      //---------------------------------------------------------------------------

      setKeyListener: function (keys) {
        var onkey = function (keyCode, mode) {
          var n, k;
          for(n = 0 ; n < keys.length ; n++) {
            k = keys[n];
            k.mode = k.mode || 'up';
            if ((k.key == keyCode) || (k.keys && (k.keys.indexOf(keyCode) >= 0))) {
              if (k.mode == mode) {
                k.action.call();
              }
            }
          }
        };
        Dom.on(document, 'keydown', function (ev) { onkey(ev.keyCode, 'down'); } );
        Dom.on(document, 'keyup',   function (ev) { onkey(ev.keyCode, 'up');   } );
      },
      //---------------------------------------------------------------------------

      playMusic: function () {
        Dom.on('mute', 'click', function () {
          if(!Dom.muted){
            Dom.muted = true;
            localStorage.setItem("muted", true)
          }
          else {
            Dom.muted = false;
            localStorage.setItem("muted", false)
          }
          Dom.toggleClassName('mute', 'on', Dom.muted);
        }, true);
      }

    }

    //=========================================================================
    // canvas rendering helpers
    //=========================================================================

    var Render = {

      polygon: function (ctx, x1, y1, x2, y2, x3, y3, x4, y4, color, myFlag) {
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.lineTo(x3, y3);
        ctx.lineTo(x4, y4);
        ctx.closePath();
        ctx.fill();
        // if(myFlag){
          // console.log(x1 +"\t"+ y1+"\t"+x2+"\t"+y2+"\t"+x3+"\t"+y3+"\t"+x4+"\t"+y4)
        // }
      },

      //---------------------------------------------------------------------------

      segment: function (ctx, width, lanes, x1, y1, w1, x2, y2, w2, fog, color) {

        var r1 = Render.rumbleWidth(w1, lanes),
            r2 = Render.rumbleWidth(w2, lanes),
            l1 = Render.laneMarkerWidth(w1, lanes),
            l2 = Render.laneMarkerWidth(w2, lanes),
            lanew1, lanew2, lanex1, lanex2, lane;
        
        ctx.fillStyle = color.grass;
        ctx.fillRect(0, y2, width, y1 - y2);
        
        Render.polygon(ctx, x1-w1-r1, y1, x1-w1, y1, x2-w2, y2, x2-w2-r2, y2, color.rumble);
        Render.polygon(ctx, x1+w1+r1, y1, x1+w1, y1, x2+w2, y2, x2+w2+r2, y2, color.rumble);
        Render.polygon(ctx, x1-w1,    y1, x1+w1, y1, x2+w2, y2, x2-w2,    y2 , color.road,true);
        
        if (color.lane) {
          lanew1 = w1*2/lanes;
          lanew2 = w2*2/lanes;
          lanex1 = x1 - w1 + lanew1;
          lanex2 = x2 - w2 + lanew2;
          for(lane = 1 ; lane < lanes ; lanex1 += lanew1, lanex2 += lanew2, lane++){
            Render.polygon(ctx, lanex1 - l1/2, y1, lanex1 + l1/2, y1, lanex2 + l2/2, y2, lanex2 - l2/2, y2, color.lane);
          }
        }
        
        Render.fog(ctx, 0, y1, width, y2-y1, fog);
      },

      //---------------------------------------------------------------------------
      //NOT USED
      // background: function (ctx, background, width, height, layer, rotation, offset) {

      //   rotation = rotation || 0;
      //   offset   = offset   || 0;

      //   var imageW = layer.w/2;
      //   var imageH = layer.h;

      //   var sourceX = layer.x + Math.floor(layer.w * rotation);
      //   var sourceY = layer.y
      //   var sourceW = Math.min(imageW, layer.x+layer.w-sourceX);
      //   var sourceH = imageH;
        
      //   var destX = 0;
      //   var destY = offset;
      //   var destW = Math.floor(width * (sourceW/imageW));
      //   var destH = height;

      //   ctx.drawImage(background, sourceX, sourceY, sourceW, sourceH, destX, destY, destW, destH);
      //   if (sourceW < imageW)
      //     ctx.drawImage(background, layer.x, sourceY, imageW-sourceW, sourceH, destW-1, destY, width-destW, destH);
      // },

      //---------------------------------------------------------------------------

      sprite: function (ctx, width, height, resolution, roadWidth, sprites, sprite, scale, destX, destY, offsetX, offsetY, clipY) {

                        //  scale for projection AND relative to roadWidth (for tweakUI)
        var destW  = (sprite.w * scale * width/2) * (SPRITES.SCALE * roadWidth);
        var destH  = (sprite.h * scale * width/2) * (SPRITES.SCALE * roadWidth);

        destX = destX + (destW * (offsetX || 0));
        destY = destY + (destH * (offsetY || 0));

        var clipH = clipY ? Math.max(0, destY+destH-clipY) : 0;
        if (clipH < destH)
          ctx.drawImage(sprites, sprite.x, sprite.y, sprite.w, sprite.h - (sprite.h*clipH/destH), destX, destY, destW, destH - clipH);
        // if(offsetY===-2){
        //   console.log(roadWidth+"\t"+sprite.h)
        // }
      },

      //---------------------------------------------------------------------------

      player: function (ctx, width, height, resolution, roadWidth, sprites, speedPercent, scale, destX, destY, steer, updown) {

        var bounce = (1.5 * Math.random() * speedPercent * resolution) * Util.randomChoice([-1,1]);
        var sprite;
        if (steer < 0)
          sprite = (updown > 0) ? SPRITES.PLAYER_UPHILL_LEFT : SPRITES.PLAYER_LEFT;
        else if (steer > 0)
          sprite = (updown > 0) ? SPRITES.PLAYER_UPHILL_RIGHT : SPRITES.PLAYER_RIGHT;
        else
          sprite = (updown > 0) ? SPRITES.PLAYER_UPHILL_STRAIGHT : SPRITES.PLAYER_STRAIGHT;

        Render.sprite(ctx, width, height, resolution, roadWidth, sprites, sprite, scale, destX, destY + bounce - 20, -0.5, -1);
      },

      //---------------------------------------------------------------------------

      fog: function (ctx, x, y, width, height, fog) {
        if (fog < 1) {
          ctx.globalAlpha = (1-fog)
          ctx.fillStyle = COLORS.FOG;
          ctx.fillRect(x, y, width, height);
          ctx.globalAlpha = 1;
        }
      },

      rumbleWidth:     function (projectedRoadWidth, lanes) { return projectedRoadWidth/Math.max(6,  2*lanes); },
      laneMarkerWidth: function (projectedRoadWidth, lanes) { return projectedRoadWidth/Math.max(32, 8*lanes); }

    }

    //=============================================================================
    // RACING GAME CONSTANTS
    //=============================================================================

    var KEY = {
      LEFT:  37,
      UP:    38,
      RIGHT: 39,
      DOWN:  40,
      A:     65,
      D:     68,
      S:     83,
      W:     87
    };

    var COLORS = {
      SKY:  '#72D7EE',
      TREE: '#005108',
      FOG:  '#005108',
      LIGHT:  { road: '#6B6B6B', grass: '#10aa10', rumble: '#ffffff', lane: '#CCCCCC'  },
      DARK:   { road: '#696969', grass: '#009a00', rumble: '#4f595e'                   },
      START:  { road: '#b72406',   grass: 'white',   rumble: 'white'                     },
      FINISH: { road: 'black',   grass: 'black',   rumble: 'black'                     }
    };

    var BACKGROUND = {
      HILLS: { x:   5, y:   5, w: 1280, h: 480 },
      SKY:   { x:   5, y: 495, w: 1280, h: 480 },
      TREES: { x:   5, y: 495, w: 1280, h: 480 }
      // TREES: { x:   5, y: 985, w: 1280, h: 480 }
    };

    var SPRITES = {
      TREE1:                  { x:   12, y:    2, w:  506, h:  418 },
      PALM_TREE:              { x:  521, y:    3, w:  330, h:  392 },
      // DEAD_TREE1:             { x:    5, y:  555, w:  135, h:  332 },
      DEAD_TREE1:             { x:  294, y:  422, w:  405, h:  347 },
      // BOULDER3:               { x:  230, y:  280, w:  320, h:  220 },
      BOULDER3:               { x:  296, y:  774, w:  353, h:  169 },
      COLUMN:                 { x:    6, y:  464, w:  252, h:  443 }, //CHANGE
      // BILLBOARD01:            { x:  625, y:  375, w:  300, h:  170 },
      // BILLBOARD06:            { x:  488, y:  555, w:  298, h:  190 },
      // BILLBOARD05:            { x:    5, y:  897, w:  298, h:  190 },
      // BILLBOARD07:            { x:  313, y:  897, w:  298, h:  190 },
      // BILLBOARD04:            { x: 1205, y:  310, w:  268, h:  170 },
      // BILLBOARD03:            { x:    5, y: 1262, w:  230, h:  220 },
      // BILLBOARD02:            { x:  245, y: 1262, w:  215, h:  220 },
      // BILLBOARD08:            { x:  230, y:    5, w:  385, h:  265 },
      // BILLBOARD09:            { x:  150, y:  555, w:  328, h:  282 },
      BILLBOARD01:            { x:   12, y:    2, w:  506, h:  418 },
      BILLBOARD06:            { x:   12, y:    2, w:  506, h:  418 },
      BILLBOARD05:            { x:   12, y:    2, w:  506, h:  418 },
      BILLBOARD07:            { x:   12, y:    2, w:  506, h:  418 },
      BILLBOARD04:            { x:   12, y:    2, w:  506, h:  418 },
      BILLBOARD03:            { x:   12, y:    2, w:  506, h:  418 },
      BILLBOARD02:            { x:   12, y:    2, w:  506, h:  418 },
      BILLBOARD08:            { x:   12, y:    2, w:  506, h:  418 },
      BILLBOARD09:            { x:   12, y:    2, w:  506, h:  418 },
      BOULDER2:               { x:  293, y:  941, w:  257, h:  148 },
      TREE2:                  { x:  853, y:    4, w:  287, h:  277 }, 
      // DEAD_TREE2:             { x: 1205, y:  490, w:  150, h:  260 },
      DEAD_TREE2:             { x:  519, y: 1058, w:  160, h:  206 },
      BOULDER1:               { x:  293, y:  941, w:  257, h:  148 },
      // BOULDER1:               { x: 1205, y:  760, w:  168, h:  248 },
      BUSH1:                  { x:  852, y: 1031, w:  264, h:  217 },
      CACTUS:                 { x:  519, y: 1058, w:  160, h:  206 },
      BUSH2:                  { x:    7, y: 1093, w:  220, h:  102 },
      STUMP:                  { x:  519, y: 1058, w:  160, h:  206 },
      // SEMI:                   { x: 1085, y:  470, w:   80, h:   53 },
      // TRUCK:                  { x: 1085, y:  470, w:   80, h:   53 },
      // CAR03:                  { x: 1085, y:  470, w:   80, h:   53 },
      // CAR02:                  { x: 1085, y:  470, w:   80, h:   53 },
      // CAR04:                  { x: 1085, y:  470, w:   80, h:   53 },
      // CAR01:                  { x: 1085, y:  470, w:   80, h:   53 },
      SEMI:                   { x:  701, y:  397, w:  151, h: 120 },
      TRUCK:                  { x:  701, y:  523, w:  151, h: 120 },
      CAR03:                  { x:  877, y:  283, w:  148, h: 114 },
      CAR02:                  { x:  869, y:  398, w:  146, h: 117 },
      CAR04:                  { x:  869, y:  525, w:  147, h: 116 },
      CAR01:                  { x:  702, y:  651, w:  145, h: 115 },
      PLAYER_UPHILL_STRAIGHT: { x:  695, y:  893, w:  146, h: 116 },
      PLAYER_UPHILL_LEFT:     { x:  692, y: 1010, w:  153, h: 118 },
      PLAYER_UPHILL_RIGHT:    { x:  855, y:  890, w:  156, h: 118 },
      // PLAYER_LEFT:     { x: 1383, y:  953, w:   84, h:   92 },
      // PLAYER_STRAIGHT: { x: 1295, y: 1018, w:   80, h:   85 },
      // PLAYER_RIGHT:    { x: 1385, y: 1018, w:   80, h:   45 }
      PLAYER_LEFT:            { x:  694, y:  774, w:  153, h:  116 },
      PLAYER_STRAIGHT:        { x:  856, y:  648, w:  149, h:  116 },
      PLAYER_RIGHT:           { x:  856, y:  775, w:  155, h:  116 },
      CLEARALLCARS:           { x:  179, y:  938, w:  102, h:  148 },
      TIME:                   { x:   36, y:  981, w:  112, h:  112 }
    };

    SPRITES.SCALE = 0.3 * (1/SPRITES.PLAYER_STRAIGHT.w) // the reference sprite width should be 1/3rd the (half-)roadWidth

    SPRITES.BILLBOARDS = [SPRITES.BILLBOARD01, SPRITES.BILLBOARD02, SPRITES.BILLBOARD03, SPRITES.BILLBOARD04, SPRITES.BILLBOARD05, SPRITES.BILLBOARD06, SPRITES.BILLBOARD07, SPRITES.BILLBOARD08, SPRITES.BILLBOARD09];
    SPRITES.PLANTS     = [SPRITES.TREE1, SPRITES.TREE2, SPRITES.DEAD_TREE1, SPRITES.DEAD_TREE2, SPRITES.PALM_TREE, SPRITES.BUSH1, SPRITES.BUSH2, SPRITES.CACTUS, SPRITES.STUMP, SPRITES.BOULDER1, SPRITES.BOULDER2, SPRITES.BOULDER3];
    SPRITES.CARS       = [SPRITES.CAR01, SPRITES.CAR02, SPRITES.CAR03, SPRITES.CAR04, SPRITES.SEMI, SPRITES.TRUCK];

    (function watchPrototype (argument) {
      // object.watch
      if (!Object.prototype.watch) {
        Object.defineProperty(Object.prototype, "watch", {
            enumerable: false
          , configurable: true
          , writable: false
          , value: function (prop, handler) {
            var oldval = this[prop], newval = oldval
            , getter = function () {
              return newval;
            }
            , setter = function (val) {
              oldval = newval;
              return newval = handler.call(this, prop, oldval, val);
            }
            ;
            
            if (delete this[prop]) { // can't watch constants
              Object.defineProperty(this, prop, {
                  get: getter
                , set: setter
                , enumerable: true
                , configurable: true
              });
            }
          }
        });
      }
       
      // object.unwatch
      if (!Object.prototype.unwatch) {
        Object.defineProperty(Object.prototype, "unwatch", {
            enumerable: false
          , configurable: true
          , writable: false
          , value: function (prop) {
            var val = this[prop];
            delete this[prop]; // remove accessors
            this[prop] = val;
          }
        });
      }
    }());

    document.body.onload = function (argument) {
      collisionSprite = false;
      engineRunning = false
      engineSound = new Howl({
        urls: ['music/engine.ogg'],
        loop: true,
        volume: 0.5
      });
      // engineSound = document.getElementById("engineMusic");
      try {
        highScore = (localStorage.getItem("highScore") == null) ? 0 : localStorage.getItem("highScore");
        document.getElementById("highScore").children[1].innerHTML = highScore;
        if(highScore == 0)
          localStorage.setItem("highScore", 0);

        Dom.muted = (localStorage.getItem("muted") == null)? false : JSON.parse(localStorage.getItem("muted"));
        if(Dom.muted == false) 
          localStorage.setItem("muted", 0);
        else {
          // Dom.toggleClassName('mute', 'on', Dom.muted);
          Dom.get("mute").className = "on";
        }
      }
      catch(e){
        console.log(e.message);
      }
      winHeight = window.innerHeight;
      winWidth = window.innerWidth;
      width          = winWidth;                    // logical canvas width
      height         = winHeight;                     // logical canvas height
      healthBar = document.getElementById("healthBar");
      gameBody = document.getElementById("gameBody");
      mainMenu = document.getElementById("mainMenu");
      pauseScreen = document.getElementById("pauseScreen");
      gameOver = document.getElementById("gameOver");
      // document.getElementById("controller").style.width = winWidth + "px";
      // document.getElementById("controller").style.height = winHeight + "px";
      healthBar.style.height = (winHeight/3) + "px";
      mainMenu.style.height = winHeight + "px";
      mainMenu.style.width = winWidth + "px";
      mainMenu.style.display = "block";
      pauseScreen.style.height = winHeight + "px";
      pauseScreen.style.width = winWidth + "px";
      gameOver.style.height = winHeight + "px";
      gameOver.style.width = winWidth + "px";
      document.getElementById("hud").style.width = winWidth + "px";
      document.getElementById("racer").style.width= winWidth + "px";
      document.getElementById("racer").style.height= winHeight + "px";
      
      document.getElementById("start").addEventListener("touchend", startGame)
      window.addEventListener("devicemotion", function (evt) {
      var  x = -evt.accelerationIncludingGravity.x;
        if(Math.round(x) > 0){
            // console.log("Right ");
            keyRight = true;

        } else if(Math.round(x) < 0) {
            // console.log("Left");
            keyLeft = true;
        } else {
            // console.log("Center");
            keyLeft = false;
            keyRight = false;
        }
      }, true);
      pauseScreen.addEventListener("click", function (argument) {
        pause = false;
        pauseScreen.style.display = "none";      
      })
      gameOver.addEventListener("click", function (argument) {
        window.location.reload();
      })
      document.addEventListener("webkitvisibilitychange", function(){
        if(gameOver.style.display == "block" ){
          window.location.reload();
        } else {
          if(!pause){
            pause = true;
            if(mainMenu.style.display == "block")
              tizen.application.getCurrentApplication().exit();
            else
              pauseScreen.style.display = "block";
          }
        }
      }, false);
      initBackListener();
      Game.playMusic();

      //Debug
      startGame();
    }

    function initBackListener () {
      // add eventListener for tizenhwkey
      document.addEventListener('tizenhwkey', function (e) {
          if(e.keyName == "back") {
              try {
                    if(mainMenu.style.display == "block")
                      tizen.application.getCurrentApplication().exit();
                    else
                    if(gameOver.style.display == "block" ){
                      window.location.reload();
                    } else {
                      if(pause){
                        pause = false;
                        if(pauseScreen.style.display == "block") {
                          window.location.reload();
                        } else
                          pauseScreen.style.display = "none";
                      } else {
                        pause = true;
                        pauseScreen.style.display = "block";
                      }
                    }
              } catch (error) {
                  console.error(error.message);
              }
          }
      });
    }
    var speedUp =   function () {
        // console.log("Moving Down");
        keyFaster = true;
        if(!engineRunning && !Dom.muted)
          engineSound.play();
    }
    var slowDown = function () {
        // console.log("Moving Down");
        keyFaster = false;
        if(!Dom.muted)
        engineSound.pause();
        engineRunning = false;
    }
    var controllerInit = function (argument) {
      document.body.addEventListener("touchstart", speedUp, true);
      document.body.addEventListener("touchend", slowDown, true);
    }
    var controllerRemove = function (argument) {
      document.body("gameBody").removeEventListener("touchstart", speedUp, true);
      document.body.removeEventListener("touchend", slowDown, true);
    }

    var fps            = 40;                      // how many 'update' frames per second
    var step           = 1/fps;                   // how long is each frame (in seconds)
    var width          = 0;                    // logical canvas width
    var height         = 0;                     // logical canvas height
    var centrifugal    = 0.3;                     // centrifugal force multiplier when going around curves
    var offRoadDecel   = 0.99;                    // speed multiplier when off road (e.g. you lose 2% speed each update frame)
    var skySpeed       = 0.001;                   // background sky layer scroll speed when going around curve (or up hill)
    var hillSpeed      = 0.002;                   // background hill layer scroll speed when going around curve (or up hill)
    var treeSpeed      = 0.003;                   // background tree layer scroll speed when going around curve (or up hill)
    var skyOffset      = 0;                       // current sky scroll offset
    var hillOffset     = 0;                       // current hill scroll offset
    var treeOffset     = 0;                       // current tree scroll offset
    var segments       = [];                      // array of road segments
    var cars           = [];                      // array of cars on the road
    var canvas         = Dom.get('canvas');       // our canvas...
    var ctx            = canvas.getContext('2d'); // ...and its drawing context
    var background     = null;                    // our background image (loaded below)
    var sprites        = null;                    // our spritesheet (loaded below)
    var resolution     = 1.2;                    // scaling factor to provide resolution independence (computed)
    var roadWidth      = 2000;                    // actually half the roads width, easier math if the road spans from -roadWidth to +roadWidth
    var segmentLength  = 200;                     // length of a single segment
    var rumbleLength   = 3;                       // number of segments per red/white rumble strip
    var trackLength    = null;                    // z length of entire track (computed)
    var lanes          = 3;                       // number of lanes
    var fieldOfView    = 100;                     // angle (degrees) for field of view
    var cameraHeight   = 500;                    // z height of camera
    var cameraDepth    = null;                    // z distance camera is from screen (computed)
    var drawDistance   = 150;                     // number of segments to draw
    var playerX        = 0;                       // player x offset from center of road (-1 to 1 to stay independent of roadWidth)
    var playerZ        = null;                    // player relative z distance from camera (computed)
    var fogDensity     = 5;                       // exponential fog density
    var position       = 0;                       // current camera Z position (add playerZ to get player's absolute Z position)
    var speed          = 10;                       // current speed
    var maxSpeed       = segmentLength/step-2000;      // top speed (ensure we can't move more than 1 segment in a single frame to make collision detection easier)
    var accel          =  maxSpeed/5;             // acceleration rate - tuned until it 'felt' right
    var breaking       = -maxSpeed;               // deceleration rate when braking
    var decel          = -maxSpeed/5;             // 'natural' deceleration rate when neither accelerating, nor braking
    var offRoadDecel   = -maxSpeed/2;             // off road deceleration is somewhere in between
    var offRoadLimit   =  maxSpeed/4;             // limit when off road deceleration no longer applies (e.g. you can always go at least this speed even when off road)
    var offRoadCounter = 0;
    var totalCars      = 200;                     // total number of cars on the road
    var currentLapTime = 0;                       // current lap time
    var lastLapTime    = null;                    // last lap time
    var curLvl         = 1;
    var keyLeft        = false;
    var keyRight       = false;
    var keyFaster      = false;
    var keySlower      = false;
    var points         = 0;
    var pause = true;
    var hud = {
      speed:            { value: null, dom: Dom.get('speed_value')            },
      current_lap_time: { value: null, dom: Dom.get('current_lap_time_value') },
      last_lap_time:    { value: null, dom: Dom.get('last_lap_time_value')    },
      fast_lap_time:    { value: null, dom: Dom.get('fast_lap_time_value')    },
      points:           { value: null, dom: Dom.get('points_value')           },
      base:             500,
      lastCheckpoint:   500,
      increment:        1,
      lvlCheck:         function () {
        if(points > (this.base*this.increment+Math.pow(this.increment,this.increment)*50)){
          maxSpeed+=1000;
          this.lastCheckpoint = this.base*this.increment+Math.pow(this.increment,this.increment)*50;
          this.increment++;
          console.log("Lvl Up!")
        } 
      }
    }

    //=========================================================================
    // UPDATE THE GAME WORLD
    //=========================================================================

    function update(dt) {
      if(!pause && Util.health>0) {
        var n, car, carW, sprite, spriteW;
        var playerSegment = findSegment(position+playerZ);
        var playerW       = SPRITES.PLAYER_STRAIGHT.w * SPRITES.SCALE;
        var speedPercent  = speed/maxSpeed;
        var dx            = dt * 2 * speedPercent; // at top speed, should be able to cross from left to right (-1 to 1) in 1 second
        var startPosition = position;

        hud.lvlCheck();

        updateCars(dt, playerSegment, playerW);

        position = Util.increase(position, dt * speed, trackLength);

        if (keyLeft)
          playerX = playerX - dx;
        else if (keyRight)
          playerX = playerX + dx;

        playerX = playerX - (dx * speedPercent * playerSegment.curve * centrifugal);

        if (keyFaster)
          speed = Util.accelerate(speed, accel, dt);
        else if (keySlower)
          speed = Util.accelerate(speed, breaking, dt);
        else
          speed = Util.accelerate(speed, decel, dt);


        if ((playerX < -1) || (playerX > 1)) {
          offRoadCounter++;
          if(offRoadCounter>=(fps*2)) {
            playerX = 0;
            offRoadCounter = 0;
            speed = 0;
          } else {
            if (speed > offRoadLimit)
              speed = Util.accelerate(speed, offRoadDecel, dt);

            //COLLISION WITH SPRITES
            // for(n = 0 ; n < playerSegment.sprites.length ; n++) {
            //   sprite  = playerSegment.sprites[n];
            //   spriteW = sprite.source.w * SPRITES.SCALE;
            //   if (Util.overlap(playerX, playerW, sprite.offset + spriteW/2 * (sprite.offset > 0 ? 1 : -1), spriteW)) {
            //     speed = maxSpeed/5;
            //     position = Util.increase(playerSegment.p1.world.z, -playerZ, trackLength); // stop in front of sprite (at front of segment)
            //     if(!collisionSprite) {
            //       Util.reduceHealth();
            //       collisionSprite = true;
            //     }
            //     break;
            //   } else
            //     collisionSprite = false;
            // }
          }
        }

        // COLLISION WITH POWERUP SPRITES
        if(playerSegment.hasPowerUp===true) {
          for(n = 0 ; n < playerSegment.sprites.length ; n++) {
            sprite  = playerSegment.sprites[n];
            // spriteW = sprite.source.w * SPRITES.SCALE;
            if (Util.overlap(playerX, playerW, sprite.offset + spriteW/2 * (sprite.offset > 0 ? 1 : -1), spriteW)) {
            //   speed = maxSpeed/5;
            // position = Util.increase(playerSegment.p1.world.z, -playerZ, trackLength); // stop in front of sprite (at front of segment)
            //   if(!collisionSprite) {
            //     Util.reduceHealth();
            //     collisionSprite = true;
            //   }
              myHud.noCollisionPower = true;
              console.log("d") 
              break;
            } 
          }
        }

        for(n = 0 ; n < playerSegment.cars.length ; n++) {
          car  = playerSegment.cars[n];
          carW = car.sprite.w * SPRITES.SCALE;
          if (speed > car.speed) {
            //COLLISION WITH OTHER CARS
            if (Util.overlap(playerX, playerW, car.offset, carW, 0.8)) {
              // console.log("crash!")
              speed    = car.speed * (car.speed/speed);
              position = Util.increase(car.z, -playerZ, trackLength);
              if(!collisionSprite) {
                Util.reduceHealth();
                collisionSprite = true;
              }
              break;
            } else
              collisionSprite = false;
          }
        }

        playerX = Util.limit(playerX, -3, 3);     // dont ever let it go too far out of bounds
        speed   = Util.limit(speed, 0, maxSpeed); // or exceed maxSpeed

        skyOffset  = Util.increase(skyOffset,  skySpeed  * playerSegment.curve * (position-startPosition)/segmentLength, 1);
        hillOffset = Util.increase(hillOffset, hillSpeed * playerSegment.curve * (position-startPosition)/segmentLength, 1);
        treeOffset = Util.increase(treeOffset, treeSpeed * playerSegment.curve * (position-startPosition)/segmentLength, 1);

        if (position > playerZ) {
          if (currentLapTime && (startPosition < playerZ)) {
            lastLapTime    = currentLapTime;
            currentLapTime = 0;
            if (lastLapTime <= Util.toFloat(Dom.storage.fast_lap_time)) {
              Dom.storage.fast_lap_time = lastLapTime;
              updateHud('fast_lap_time', formatTime(lastLapTime));
              Dom.addClassName('fast_lap_time', 'fastest');
              Dom.addClassName('last_lap_time', 'fastest');
            }
            else {
              Dom.removeClassName('fast_lap_time', 'fastest');
              Dom.removeClassName('last_lap_time', 'fastest');
            }
            updateHud('last_lap_time', formatTime(lastLapTime));
            Dom.show('last_lap_time');
          }
          else {
            currentLapTime += dt;
          }
        }

        updateHud('speed',            5 * Math.round(speed/500));
        points += (1 * Math.round(speed/5000));
        updateHud('points',            points);
        updateHud('current_lap_time', formatTime(currentLapTime));

      } else if(Util.health === 0){
        gameOver.style.display = "block";
        if(points > highScore)
          localStorage.setItem("highScore", points);
      }
    }

    //-------------------------------------------------------------------------

    function updateCars(dt, playerSegment, playerW) {
      var n, car, oldSegment, newSegment;
      for(n = 0 ; n < cars.length ; n++) {
        car         = cars[n];
        oldSegment  = findSegment(car.z);
        car.offset  = car.offset + updateCarOffset(car, oldSegment, playerSegment, playerW);
        car.z       = Util.increase(car.z, dt * car.speed, trackLength);
        car.percent = Util.percentRemaining(car.z, segmentLength); // useful for interpolation during rendering phase
        newSegment  = findSegment(car.z);
        if (oldSegment != newSegment) {
          var index = oldSegment.cars.indexOf(car);
          oldSegment.cars.splice(index, 1);
          newSegment.cars.push(car);
        }
      }
    }

    function updateCarOffset(car, carSegment, playerSegment, playerW) {
      //MAIN LOOP

      var i, j, dir, segment, otherCar, otherCarW, lookahead = 20, carW = car.sprite.w * SPRITES.SCALE;

      // optimization, dont bother steering around other cars when 'out of sight' of the player
      if ((carSegment.index - playerSegment.index) > drawDistance)
        return 0;

      for(i = 1 ; i < lookahead ; i++) {
        segment = segments[(carSegment.index+i)%segments.length];

        if ((segment === playerSegment) && (car.speed > speed) && (Util.overlap(playerX, playerW, car.offset, carW, 1.2))) {
          if (playerX > 0.5)
            dir = -1;
          else if (playerX < -0.5)
            dir = 1;
          else
            dir = (car.offset > playerX) ? 1 : -1;
          return dir * 1/i * (car.speed-speed)/maxSpeed; // the closer the cars (smaller i) and the greated the speed ratio, the larger the offset
        }

        for(j = 0 ; j < segment.cars.length ; j++) {
          otherCar  = segment.cars[j];
          otherCarW = otherCar.sprite.w * SPRITES.SCALE;
          if ((car.speed > otherCar.speed) && Util.overlap(car.offset, carW, otherCar.offset, otherCarW, 1.2)) {
            if (otherCar.offset > 0.5)
              dir = -1;
            else if (otherCar.offset < -0.5)
              dir = 1;
            else
              dir = (car.offset > otherCar.offset) ? 1 : -1;
            return dir * 1/i * (car.speed-otherCar.speed)/maxSpeed;
          }
        }
      }

      // if no cars ahead, but I have somehow ended up off road, then steer back on
      if (car.offset < -0.9)
        return 0.1;
      else if (car.offset > 0.9)
        return -0.1;
      else
        return 0;
    }

    //-------------------------------------------------------------------------

    function updateHud(key, value) { // accessing DOM can be slow, so only do it if value has changed
      if (hud[key].value !== value) {
        hud[key].value = value;
        Dom.set(hud[key].dom, value);
      }
    }

    function formatTime(dt) {
      var minutes = Math.floor(dt/60);
      var seconds = Math.floor(dt - (minutes * 60));
      var tenths  = Math.floor(10 * (dt - Math.floor(dt)));
      if (minutes > 0)
        return minutes + "." + (seconds < 10 ? "0" : "") + seconds + "." + tenths;
      else
        return seconds + "." + tenths;
    }

    //=========================================================================
    // RENDER THE GAME WORLD
    //=========================================================================

    function render() {
      // console.log("sdf")
      if(!pause) {
        var baseSegment   = findSegment(position);
        var basePercent   = Util.percentRemaining(position, segmentLength);
        var playerSegment = findSegment(position+playerZ);
        var playerPercent = Util.percentRemaining(position+playerZ, segmentLength);
        var playerY       = Util.interpolate(playerSegment.p1.world.y, playerSegment.p2.world.y, playerPercent);
        var maxy          = height;

        var x  = 0;
        var dx = - (baseSegment.curve * basePercent);

        ctx.clearRect(0, 0, width, height);

        // Render.background(ctx, background, width, height, BACKGROUND.SKY,   skyOffset,  resolution * skySpeed  * playerY);
        // Render.background(ctx, background, width, height, BACKGROUND.HILLS, hillOffset, resolution * hillSpeed * playerY);
        // Render.background(ctx, background, width, height, BACKGROUND.TREES, treeOffset, resolution * treeSpeed * playerY);

        var n, i, segment, car, sprite, spriteScale, spriteX, spriteY;

        for(n = 0 ; n < drawDistance ; n++) {

          segment        = segments[(baseSegment.index + n) % segments.length];
          segment.looped = segment.index < baseSegment.index;
          segment.fog    = Util.exponentialFog(n/drawDistance, fogDensity);
          segment.clip   = maxy;

          Util.project(segment.p1, (playerX * roadWidth) - x,      playerY + cameraHeight, position - (segment.looped ? trackLength : 0), cameraDepth, width, height, roadWidth);
          Util.project(segment.p2, (playerX * roadWidth) - x - dx, playerY + cameraHeight, position - (segment.looped ? trackLength : 0), cameraDepth, width, height, roadWidth);

          x  = x + dx;
          dx = dx + segment.curve;

          if ((segment.p1.camera.z <= cameraDepth)         || // behind us
              (segment.p2.screen.y >= segment.p1.screen.y) || // back face cull
              (segment.p2.screen.y >= maxy))                  // clip by (already rendered) hill
            continue;

          Render.segment(ctx, width, lanes,
                         segment.p1.screen.x,
                         segment.p1.screen.y,
                         segment.p1.screen.w,
                         segment.p2.screen.x,
                         segment.p2.screen.y,
                         segment.p2.screen.w,
                         segment.fog,
                         segment.color);

          maxy = segment.p1.screen.y;
        }
// var tSprite;
        for(n = (drawDistance-1) ; n > 0 ; n--) {
          segment = segments[(baseSegment.index + n) % segments.length];

          for(i = 0 ; i < segment.cars.length ; i++) {
            //RENDER CARS
            car         = segment.cars[i];
            sprite      = car.sprite;
            spriteScale = Util.interpolate(segment.p1.screen.scale, segment.p2.screen.scale, car.percent);
            spriteX     = Util.interpolate(segment.p1.screen.x,     segment.p2.screen.x,     car.percent) + (spriteScale * car.offset * roadWidth * width/2);
            spriteY     = Util.interpolate(segment.p1.screen.y,     segment.p2.screen.y,     car.percent);
            // console.log(car)
            Render.sprite(ctx, width, height, resolution, roadWidth, sprites, car.sprite, spriteScale, spriteX, spriteY, -0.5, -1, segment.clip);
        
          }

          for(i = 0 ; i < segment.sprites.length ; i++) {
            //RENDER REMAINING SPRITES
            sprite      = segment.sprites[i];
            spriteScale = segment.p1.screen.scale;
            spriteX     = segment.p1.screen.x + (spriteScale * sprite.offset * roadWidth * width/2);
            spriteY     = segment.p1.screen.y;
            if(sprite.x>=100){

              Render.sprite(ctx, width, height, resolution, roadWidth, sprites, sprite.source, spriteScale, sprite.x, spriteY, (sprite.offset < 0 ? -1 : 0), -1, segment.clip);
            } else 
              Render.sprite(ctx, width, height, resolution, roadWidth, sprites, sprite.source, spriteScale, spriteX, spriteY, (sprite.offset < 0 ? -1 : 0), -1, segment.clip);
            // if(sprite.source.x===5 &&sprite.source.y===5)
            //   tSprite = sprite.source;
          }

          if (segment == playerSegment) {
            //RENDER PLAYER CAR
            Render.player(ctx, width, height, resolution, roadWidth, sprites, speed/maxSpeed,
                          cameraDepth/playerZ,
                          width/2,
                          (height/2) - (cameraDepth/playerZ * Util.interpolate(playerSegment.p1.camera.y, playerSegment.p2.camera.y, playerPercent) * height/2),
                          speed * (keyLeft ? -1 : keyRight ? 1 : 0),
                          playerSegment.p2.world.y - playerSegment.p1.world.y);
          }
        }
        //DEBUGGING - RENDER tSprite
        // Render.sprite(ctx, width, height, resolution, roadWidth, sprites, tSprite, 0.000088540803781540137, myDestX,  myDestY,  myOffsetX, myOffsetY , myClipy);
      }
    }

    function findSegment(z) {
      return segments[Math.floor(z/segmentLength) % segments.length]; 
    }

    //=========================================================================
    // BUILD ROAD GEOMETRY
    //=========================================================================

    function lastY() { return (segments.length == 0) ? 0 : segments[segments.length-1].p2.world.y; }

    function addSegment(curve, y) {

      var n = segments.length;
      myAddSegment++;
      segments.push({
          index: n,
             p1: { world: { y: lastY(), z:  n   *segmentLength }, camera: {}, screen: {} },
             p2: { world: { y: y,       z: (n+1)*segmentLength }, camera: {}, screen: {} },
          curve: curve,
        sprites: [],
           cars: [],
          color: Math.floor(n/rumbleLength)%2 ? COLORS.DARK : COLORS.LIGHT,
     hasPowerUp: false
      });
    }

    function addSprite(n, sprite, offset, x) {
      segments[n].sprites.push({ source: sprite, offset: offset, x: x||0 });
    }

    function addRoad(enter, hold, leave, curve, y) {
      var startY   = lastY();
      var endY     = startY + (Util.toInt(y, 0) * segmentLength);
      var n, total = enter + hold + leave;
      for(n = 0 ; n < enter ; n++)
        addSegment(Util.easeIn(0, curve, n/enter), Util.easeInOut(startY, endY, n/total));
      for(n = 0 ; n < hold  ; n++)
        addSegment(curve, Util.easeInOut(startY, endY, (enter+n)/total));
      for(n = 0 ; n < leave ; n++)
        addSegment(Util.easeInOut(curve, 0, n/leave), Util.easeInOut(startY, endY, (enter+hold+n)/total));
    }

    var ROAD = {
      LENGTH: { NONE: 0, SHORT:  25, MEDIUM:   50, LONG:  100 },
      HILL:   { NONE: 0, LOW:    20, MEDIUM:   40, HIGH:   60 },
      CURVE:  { NONE: 0, EASY:    2, MEDIUM:    4, HARD:    6 }
    };

    function addStraight(num) {
      num = num || ROAD.LENGTH.MEDIUM;
      addRoad(num, num, num, 0, 0);
    }

    function addHill(num, height) {
      num    = num    || ROAD.LENGTH.MEDIUM;
      height = height || ROAD.HILL.MEDIUM;
      addRoad(num, num, num, 0, height);
    }

    function addCurve(num, curve, height) {
      num    = num    || ROAD.LENGTH.MEDIUM;
      curve  = curve  || ROAD.CURVE.MEDIUM;
      height = height || ROAD.HILL.NONE;
      addRoad(num, num, num, curve, height);
    }
        
    function addLowRollingHills(num, height) {
      num    = num    || ROAD.LENGTH.SHORT;
      height = height || ROAD.HILL.LOW;
      addRoad(num, num, num,  0,                height/2);
      addRoad(num, num, num,  0,               -height);
      addRoad(num, num, num,  ROAD.CURVE.EASY,  height);
      addRoad(num, num, num,  0,                0);
      addRoad(num, num, num, -ROAD.CURVE.EASY,  height/2);
      addRoad(num, num, num,  0,                0);
    }

    function addSCurves() {
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,  -ROAD.CURVE.EASY,    ROAD.HILL.NONE);
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,   ROAD.CURVE.MEDIUM,  ROAD.HILL.MEDIUM);
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,   ROAD.CURVE.EASY,   -ROAD.HILL.LOW);
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,  -ROAD.CURVE.EASY,    ROAD.HILL.MEDIUM);
      addRoad(ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM, ROAD.LENGTH.MEDIUM,  -ROAD.CURVE.MEDIUM, -ROAD.HILL.MEDIUM);
    }

    function addBumps() {
      addRoad(10, 10, 10, 0,  5);
      addRoad(10, 10, 10, 0, -2);
      addRoad(10, 10, 10, 0, -5);
      addRoad(10, 10, 10, 0,  8);
      addRoad(10, 10, 10, 0,  5);
      addRoad(10, 10, 10, 0, -7);
      addRoad(10, 10, 10, 0,  5);
      addRoad(10, 10, 10, 0, -2);
    }

    function addDownhillToEnd(num) {
      num = num || 200;
      addRoad(num, num, num, -ROAD.CURVE.EASY, -lastY()/segmentLength);
    }

    function resetRoad() {
      segments = [];

      addStraight(ROAD.LENGTH.SHORT);                                   // 0-25-50-75
      addLowRollingHills();                                             // 75-525
      addSCurves();                                                     // 525-1275
      addCurve(ROAD.LENGTH.MEDIUM, ROAD.CURVE.MEDIUM, ROAD.HILL.LOW);   // 1425
      addBumps();                                                       // 1665
      addLowRollingHills();                                             // 2115
      addCurve(ROAD.LENGTH.LONG*2, ROAD.CURVE.MEDIUM, ROAD.HILL.MEDIUM);// 2715
      addStraight();                                                    // 2865 
      addHill(ROAD.LENGTH.MEDIUM, ROAD.HILL.HIGH);                      // 3015
      addSCurves();
      addCurve(ROAD.LENGTH.LONG, -ROAD.CURVE.MEDIUM, ROAD.HILL.NONE);
      addHill(ROAD.LENGTH.LONG, ROAD.HILL.HIGH);
      addCurve(ROAD.LENGTH.LONG, ROAD.CURVE.MEDIUM, -ROAD.HILL.LOW);
      addBumps();
      addHill(ROAD.LENGTH.LONG, -ROAD.HILL.MEDIUM);
      addStraight();
      addSCurves();
      addDownhillToEnd();                                               // 6705

      resetSprites();
      resetCars();

      segments[findSegment(playerZ).index + 2].color = COLORS.START;
      segments[findSegment(playerZ).index + 3].color = COLORS.START;
      for(var n = 0 ; n < rumbleLength ; n++)
        segments[segments.length-1-n].color = COLORS.FINISH;

      trackLength = segments.length * segmentLength ;
    }

    function resetSprites() {
      var n, i;

      addSprite(20,  SPRITES.BILLBOARD07, -1);
      addSprite(40,  SPRITES.BILLBOARD06, -1);
      addSprite(60,  SPRITES.BILLBOARD08, -1);
      addSprite(80,  SPRITES.BILLBOARD09, -1);
      addSprite(100, SPRITES.BILLBOARD01, -1);
      addSprite(120, SPRITES.BILLBOARD02, -1);
      addSprite(140, SPRITES.BILLBOARD03, -1);
      addSprite(160, SPRITES.BILLBOARD04, -1);
      addSprite(180, SPRITES.BILLBOARD05, -1);

      addSprite(240,                  SPRITES.BILLBOARD07, -1.2);
      addSprite(240,                  SPRITES.BILLBOARD06,  1.2);
      addSprite(segments.length - 25, SPRITES.BILLBOARD07, -1.2);
      addSprite(segments.length - 25, SPRITES.BILLBOARD06,  1.2);

      for(n = 10 ; n < 200 ; n += 4 + Math.floor(n/100)) {
        addSprite(n, SPRITES.PALM_TREE, 1 + Math.random()*0.5);
        addSprite(n, SPRITES.PALM_TREE,   1 + Math.random()*2);
      }

      for(n = 250 ; n < 1000 ; n += 5) {
        addSprite(n,     SPRITES.COLUMN, 1.1);
        addSprite(n + Util.randomInt(0,5), SPRITES.TREE1, -1 - (Math.random() * 2));
        addSprite(n + Util.randomInt(0,5), SPRITES.TREE2, -1 - (Math.random() * 2));
      }

      for(n = 200 ; n < segments.length ; n += 3) {
        addSprite(n, Util.randomChoice(SPRITES.PLANTS), Util.randomChoice([1,-1]) * (2 + Math.random() * 5));
      }

      var side, sprite, offset;
      for(n = 1000 ; n < (segments.length-50) ; n += 100) {
        side      = Util.randomChoice([1, -1]);
        addSprite(n + Util.randomInt(0, 50), Util.randomChoice(SPRITES.BILLBOARDS), -side);
        for(i = 0 ; i < 20 ; i++) {
          sprite = Util.randomChoice(SPRITES.PLANTS);
          offset = side * (1.5 + Math.random());
          addSprite(n + Util.randomInt(0, 50), sprite, offset);
        }
          
      }

      //Adding custom sprite
      addSprite(49,SPRITES.CLEARALLCARS,-1, 200);
      segments[49].hasPowerUp=true;
    }

    function resetCars() {
      cars = [];
      var n, car, segment, offset, z, sprite, speed;
      for (var n = 0 ; n < totalCars ; n++) {
        offset = Math.random() * Util.randomChoice([-0.8, 0.8]);
        z      = Math.floor(Math.random() * segments.length) * segmentLength;
        sprite = Util.randomChoice(SPRITES.CARS);
        speed  = maxSpeed/4 + Math.random() * maxSpeed/(sprite == SPRITES.SEMI ? 4 : 2);
        car = { offset: offset, z: z, sprite: sprite, speed: speed };
        segment = findSegment(car.z);
        segment.cars.push(car);
        cars.push(car);
      }
    }

    //=========================================================================
    // THE GAME LOOP
    //=========================================================================
    function startGame () {
      myHud.initWatchListeners();
      controllerInit();
      if(!Dom.muted)
      document.getElementById("startMusic").play();
      pause = false;
      setTimeout(function (argument) {
        mainMenu.style.display = "none";
        gameBody.style.display = "block";
      }, 1480);

      Game.run({
        canvas: canvas, render: render, update: update, step: step,
        images: ["background", "sprites"],
        keys: [
          { keys: [KEY.LEFT,  KEY.A], mode: 'down', action: function () { keyLeft   = true;  } },
          { keys: [KEY.RIGHT, KEY.D], mode: 'down', action: function () { keyRight  = true;  } },
          { keys: [KEY.UP,    KEY.W], mode: 'down', action: function () { keyFaster = true;  } },
          { keys: [KEY.DOWN,  KEY.S], mode: 'down', action: function () { keySlower = true;  } },
          { keys: [KEY.LEFT,  KEY.A], mode: 'up',   action: function () { keyLeft   = false; } },
          { keys: [KEY.RIGHT, KEY.D], mode: 'up',   action: function () { keyRight  = false; } },
          { keys: [KEY.UP,    KEY.W], mode: 'up',   action: function () { keyFaster = false; } },
          { keys: [KEY.DOWN,  KEY.S], mode: 'up',   action: function () { keySlower = false; } }
        ],
        ready: function (images) {
          background = images[0];
          sprites    = images[1];
          reset();
          Dom.storage.fast_lap_time = Dom.storage.fast_lap_time || 180;
          updateHud('fast_lap_time', formatTime(Util.toFloat(Dom.storage.fast_lap_time)));
        }
      });  
    }


    function reset(options) {
      options       = options || {};
      canvas.width  = width  = Util.toInt(options.width,          width);
      canvas.height = height = Util.toInt(options.height,         height);
      lanes                  = Util.toInt(options.lanes,          lanes);
      roadWidth              = Util.toInt(options.roadWidth,      roadWidth);
      cameraHeight           = Util.toInt(options.cameraHeight,   cameraHeight);
      drawDistance           = Util.toInt(options.drawDistance,   drawDistance);
      fogDensity             = Util.toInt(options.fogDensity,     fogDensity);
      fieldOfView            = Util.toInt(options.fieldOfView,    fieldOfView);
      segmentLength          = Util.toInt(options.segmentLength,  segmentLength);
      rumbleLength           = Util.toInt(options.rumbleLength,   rumbleLength);
      cameraDepth            = 1 / Math.tan((fieldOfView/2) * Math.PI/180);
      playerZ                = (cameraHeight * cameraDepth);
      resolution             = height/480;

      if ((segments.length==0) || (options.segmentLength) || (options.rumbleLength))
        resetRoad(); // only rebuild road when necessary
    }

    //=========================================================================
    // TWEAK UI HANDLERS
    //=========================================================================


    //=========================================================================

